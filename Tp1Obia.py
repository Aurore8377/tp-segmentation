# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

import numpy as np
#import gdal
#from osgeo import gdal_array
import skimage.io as io
from PIL import Image
import otbApplication

#def read_image(path):
#    img = gdal.Open(path)
#    nbands = img.RasterCount
#    gt=img.GetGeoTransform()
#    prj=img.GetProjection()
#    data_type=img.GetRasterBand(1).DataType
#    width,height=img.RasterXSize, img.RasterYSize
#    res=np.zeros((nbands, img.RasterYSize,img.RasterXSize))
#    for bands in range(nbands):
#        res[bands]=img.GetRasterBand(bands+1).ReadAsArray()
#    img=None
#    return res,{"gt":gt,"prj":prj,"width":width,"height":height,"data_type":data_type}
#
#def save_image(path,arr,metadata):
#    driver = gdal.GetDriverByName('GTiff')
#    dataset = driver.Create(path, np.shape(arr)[2], np.shape(arr)[1], np.shape(arr)[0], gdal_array.NumericTypeCodeToGDALTypeCode(arr.dtype.type))
#    if metadata["gt"]:
#        dataset.SetGeoTransform(metadata["gt"])
#    if metadata["prj"]:
#        dataset.SetProjection(metadata["prj"])
#    for i in range(np.shape(arr)[0]):
#        print(i,arr[i].shape,np.shape(arr)[1], np.shape(arr)[2])
#        dataset.GetRasterBand(i+1).WriteArray(arr[i])
#    dataset.FlushCache()
#    dataset = None


def module_segmentation(chemin_im_entree):
    
    im_entree=Image.open(chemin_im_entree)
    im_entree= Image.fromarray(im_entree)
    red, green, blue = im_entree.split()
    red.save("rouge.tif")
    
    app = otbApplication.Registry.CreateApplication("Segmentation")

    app.SetParameterString("in", "rouge.tif")
    app.SetParameterString("mode","vector")
    app.SetParameterString("mode.vector.out", "SegmentationVector.gml")#image de sortie en .gml(vecteur)
    app.SetParameterString("filter","watershed")

    app.ExecuteAndWriteOutput()



def partage_eaux(chemin_input, output, model):#input : image à classifier, output:chemin de l'image de sortie que l'on souhaite donner, model : modèle
    L=[]
    somme=0
    im_entree=Image.open(chemin_input)
    red, green, blue = im_entree.split()
    for i in range(len(red)):
        L.append((red[i]+green[i]+blue[i])**2)
    for j in range(len(L)):
        somme+=L[j]
    altitude=np.sqrt(somme)
    profondeur=
 
            
if __name__ == "__main__":
    
#     print(  

    
    