# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

import numpy as np
#import gdal
#from osgeo import gdal_array
import skimage.io as io
from PIL import Image
import otbApplication

#def read_image(path):
#    img = gdal.Open(path)
#    nbands = img.RasterCount
#    gt=img.GetGeoTransform()
#    prj=img.GetProjection()
#    data_type=img.GetRasterBand(1).DataType
#    width,height=img.RasterXSize, img.RasterYSize
#    res=np.zeros((nbands, img.RasterYSize,img.RasterXSize))
#    for bands in range(nbands):
#        res[bands]=img.GetRasterBand(bands+1).ReadAsArray()
#    img=None
#    return res,{"gt":gt,"prj":prj,"width":width,"height":height,"data_type":data_type}
#
#def save_image(path,arr,metadata):
#    driver = gdal.GetDriverByName('GTiff')
#    dataset = driver.Create(path, np.shape(arr)[2], np.shape(arr)[1], np.shape(arr)[0], gdal_array.NumericTypeCodeToGDALTypeCode(arr.dtype.type))
#    if metadata["gt"]:
#        dataset.SetGeoTransform(metadata["gt"])
#    if metadata["prj"]:
#        dataset.SetProjection(metadata["prj"])
#    for i in range(np.shape(arr)[0]):
#        print(i,arr[i].shape,np.shape(arr)[1], np.shape(arr)[2])
#        dataset.GetRasterBand(i+1).WriteArray(arr[i])
#    dataset.FlushCache()
#    dataset = None

##EXERCICE 1:

im_seg=io.imread('IRC_Segmentation.tif')
im_class=io.imread('IRC_Classif.tif')

def nbc(im_class):
#    nb_class=im_class[0][0]
#    for i in im_class:
#        for j in range(len(im_class)):
#            if i[j] <= i[j+1] and i[j+1]>=nb_class:
#                nb_class=i[j+1]
#    return  nb_class
    List=np.unique(im_class);
    return len(List)
        
def nbs(im_seg):
    #print(np.shape(im_seg))
    List=np.unique(im_seg)
    return len(List)
                

def init_M(im_class, im_seg):
    c= nbc(im_class)
    s= nbs(im_seg)
    return np.zeros((c,s))


def M(im_class,im_seg):
    M=init_M(im_class,im_seg)
    liste, corresp = np.unique(im_seg,return_inverse=True)
    corresp=corresp.reshape((np.shape(im_seg)))
    for i in range(len(im_class)):
        for j in range (len(im_class[i])):
            classe = im_class[i][j]
            segment=corresp[i][j]
            M[classe-1][segment-1]+=1
    return M
        
        
  #V colonne de taille (nbs) et récupérer l'identifiant de la valeur max pour chaque segment 
    
def V(im_class,im_seg):
    
    Ma=M(im_class,im_seg)
    N=len(Ma[0]) #longueur de chaque ligne
    Vect = np.zeros(N)
    for i in range(N):
        colonnes=Ma[:,i]
        max_col=np.max(colonnes)
        print(max_col)
        Vect[i] = max_col
    return Vect
    
# def image(im_class,im_seg):
#     (w,h)=np.shape(im_seg)
#     new_im=Image.new("L",(h,w))
#     image_vide=new_im.io.imsave("image_vide.png","PNG")
#     Vect=V(im_class,im_seg)
#     for i in image_vide:
#        for j in range(len(image_vide[i])):
#            image_vide[i][j]=Vect[i] 
#     image_remplie=image_vide.io.imsave("image_remplie.png","PNG")
#     return image_remplie
#     
 
    

#def classification_pixellaire(im_seg,im_class):

##EXERCICE 2:


def recuperation_canaux_moyenne(irc, irc_seg):
    image(irc,irc_seg)#On calcul les calsses grâce aux méthodes du premier exo
    im_remplie=Image.open("image_remplie.png")#générée grâce à la fonction précédente
    im_remplie= Image.fromarray(im_remplie)
    red, green, blue = im_remplie.split()#dans une composition irc les canaux sont utilisés de la façon suivante red=i green=r blue=c
    pixel_moy=[]
    for i in range(len(red)):
        pixel_moy.append((red[i]+green[i]+blue[i])/3)
    new_im=Image.new("L",(h,w))
    for i in range(len(pixel_moy):
        new_im[i]=pixel_moy[i]
    new_im.save("canaux_moy.png")
 

       
def image_calssification(input, output, model):#input : image à classifier, output:chemin de l'image de sortie que l'on souhaite donner, model : modèle

    app = otbApplication.Registry.CreateApplication("ImageClassifier")

    app.SetParameterString("in", input)
    app.SetParameterString("model", model)
    app.SetParameterString("out", output)

    app.ExecuteAndWriteOutput()
            
if __name__ == "__main__":
    
##EXO1

#     print(nbc(im_class))
#     print(nbs(im_seg))    
#     print(init_M(im_class,im_seg))      
            
#     print(M(im_class,im_seg))
            
#    print(V(im_class,im_seg))
    
#    print(image(im_class,im_seg))


##EXO2
#    recuperation_canaux_moyenne("imageIRC.tif", "IRC_seg.tif")#génération de "canaux_moy.png"
#    image_classifiction("canaux_moy.png", "output.png", "classification_oraison.model")#génération de l'image clssifiée
    
    
    